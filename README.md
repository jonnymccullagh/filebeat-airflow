# Filebeat Airflow Module

A simple module for processing airflow logs with Filebeat

## Usage

Copy the contents of ./module/airflow to the path (-path.home) used 
for modules e.g. /usr/share/filebeat/module/ 

```
cp -R ./module/airflow /usr/share/filebeat/module
```
Then enable the module by copying the contents of ./modules.d/airflow.yml 
to your config directory e.g. /etc/filebeat/modules.d

```
cp ./modules.d/airflow.yml /etc/filebeat/modules.d/airflow.yml
```
Amend the airflow.yml file to point to the location of your log files. 
Restart filebeat e.g.:
```
systemctl restart filebeat
```

## License

This project is licensed under the MIT License

## Acknowledgments

Thanks to André Letterer on the Elastic forums.
